<?php

namespace ObjectStream;

use \Countable;

/**
 * Defines a browsable object stream.
 * 
 * The Iterator interface for this class is being used for browsing into the
 * current result page, while the Countable::count() method should return the
 * total item count (if no paging would have been done).
 * 
 * Having the exact count is not something mandatory, you can return a count
 * approximative result if you want, but it'd probably disturb pagers.
 */
interface BrowseableStreamInterface
  extends StreamInterface, Countable
{
  /**
   * No limit.
   * 
   * @var int
   */
  const NO_LIMIT = 0;

  /**
   * Set current query limit.
   * 
   * @param int $limit
   *   You can pass ObjectStream_Browseable_Interface::NO_LIMIT here for no
   *   limit.
   * 
   * @return ObjectStream_Browseable_Interface
   *   Self reference for chaining.
   */
  public function setLimit($limit);

  /**
   * Get current query limit.
   * 
   * @return int
   */
  public function getLimit();

  /**
   * Set current offset. Offset is not the current cursor position of the
   * Iterator interface, it's the offset being used for querying, if it has
   * any sens.
   * 
   * @param int $offset
   * 
   * @return ObjectStream_Browseable_Interface
   *   Self reference for chaining.
   */
  public function setOffset($offset);

  /**
   * Get current offset. Offset is not the current cursor position of the
   * Iterator interface, it's the offset being used for querying, if it has
   * any sens.
   * 
   * @return int
   */
  public function getOffset();

  /**
   * Alias to both setOffset() and setLimit() methods.
   * 
   * @param int $offset = NULL
   *   (optional) Leave to NULL for no change of the current value.
   * @param int $limit = NULL
   *   (optional) Leave to NULL for no change of the current value.
   * 
   * @return ObjectStream_Browseable_Interface
   *   Self reference for chaining.
   */
  public function setRange($offset = NULL, $limit = NULL);
}
