<?php

namespace ObjectStream;

use \Traversable;

/**
 * Object stream interface.
 */
interface StreamInterface extends Traversable
{
  /**
   * Get object datatype.
   * 
   * @return string
   */
  public function getDatatype();
}
