<?php

namespace ObjectStream;

use \Oox_Registrable_Abstract as RegistrableAbstract;

use Schema\ExportableInterface,
    Schema\ReadonlySchemaInterface,
    Schema\SchemaArray;

use \Iterator;
    
/**
 * This implementation will be able to fetch any kind of entity using the
 * EntityFieldQuery class as backend.
 * 
 * This implementation provides a static caching mecanism which allows you
 * to iterate over fetched objects more than once. Each time you modify any
 * of this object's properties, this cache is cleared properly and query is
 * run again.
 * 
 * Querying a non existing entity type will fail at query time, not before.
 * This is your responsability to ensure that you are actually querying an
 * existing entity type.
 * 
 * <code>
 *   use ObjectStream\Entity\EntityQueryStream;
 * 
 *   // Basic node example.
 *   $stream = new ObjectStream\Entity\EntityQueryStream;
 *   $stream
 *     ->setEntityType('node')
 *     ->setRange(2, 10)
 *   foreach ($stream as $node) {
 *     // Do something.
 *   }
 *   
 *   We could even render those directly:
 *   return node_view_multiple(new ObjectStream_Entity_Query('node', 0, 10));
 * 
 *   // Basic user example.
 *   $stream = new ObjectStream_Entity_Query;
 *   $stream
 *     ->setEntityType('user')
 *     ->setRange(1, 16)
 *   foreach ($stream as $user) {
 *     // Do something.
 *   }
 * </code>
 */
class EntityQueryStream
  extends RegistrableAbstract
  implements Iterator, BrowseableStreamInterface, ExportableInterface
{
  /**
   * @var string
   */
  protected $entityType;

  /**
   * @var array
   */
  protected $objects;

  /**
   * @var int
   */
  protected $count;

  /**
   * @var int
   */
  protected $pos = 0;

  /**
   * @var boolean
   */
  protected $modified = FALSE;

  /**
   * @var EntityFieldQuery
   */
  protected $query;

  /**
   * Alias of the getEntityType() method.
   * 
   * @see StreamInterface::getDatatype()
   */
  public function getDatatype() {
    return $this->entityType;
  }

  /**
   * Set entity type for querying.
   * 
   * This entity type will be exposed as stream's datatype.
   * 
   * @param string $entityType
   * 
   * @return EntityQueryStream
   */
  public function setEntityType($entityType) {
    $this->entityType = $entityType;
    return $this;
  }

  /**
   * Get entity type.
   * 
   * @return string
   */
  public function getEntityType() {
    return $this->entityType;
  }

  /**
   * @var int
   */
  protected $limit = BrowseableStreamInterface::NO_LIMIT;

  public function setLimit($limit) {
    $this->limit = $limit;
    return $this;
  }

  public function getLimit() {
    return $this->limit;
  }

  /**
   * @var int
   */
  protected $offset = 0;

  public function setOffset($offset) {
    $this->offset = $offset;
    return $this;
  }

  public function getOffset() {
    return $this->offset;
  }

  public function setRange($offset = NULL, $limit = NULL) {
    if (isset($offset)) {
      $this->offset = $offset;
    }
    if (isset($limit)) {
      $this->limit = $limit;
    }
  }

  /**
   * Prepare and the query.
   */
  protected function initQuery() {
    if ($this->modified || !isset($this->query)) {
      // Clean up internals.
      $this->modified = FALSE;

      // Prepare the field query.
      $this->query = new EntityFieldQuery;
      $this->query->entityCondition('entity_type', $this->getDatatype());

      /*
      foreach ($this->_options['filters'] as $filter) {
        $filter->alterQuery($this->_query);
      }
       */
    }
  }

  public function count() {
    if ($this->modified || !isset($this->count)) {
      $this->initQuery();

      // Clone the actual query in order to get a count query.
      // Once again, this behavior differs from the actual SelectQuery from
      // DBTNG, which is quite a pain in the ass.
      $countQuery = clone $this->query;
      $this->count = $countQuery->count()->execute();
    }

    return $this->count;
  }

  /**
   * Query the Drupal entity storage and fetch entities.
   * 
   * @return EntityQueryStream
   */
  public function executeQuery() {
    if ($this->modified || !isset($this->objects)) {
      $this->initQuery();

      // Set limit and offset for paging requests.
      if ($this->limit || $this->offset) {
        $this->query->range($this->offset, $this->limit);
      }

      // Use the entity system to query objects.
      $this->objects = array();
      $entity = entity_get_controller($this->entityType);
      $result = $this->query->execute();

      // This one was tricky.
      $this->objects = $entity->load(array_keys($result[$this->entityType]));
    }
    return $this;
  }

  public function current() {
    if (!isset($this->objects)) {
      $this->initQuery();
    }
    return current($this->objects);
  }

  public function key() {
    if (!isset($this->objects)) {
      $this->initQuery();
    }
    return key($this->objects);
  }

  public function next() {
    if (!isset($this->objects)) {
      $this->initQuery();
    }
    next($this->objects);
    return $this->pos++;
  }

  public function rewind() {
    if (!isset($this->objects)) {
      $this->executeQuery();
    }
    reset($this->objects);
    $this->pos = 0;
  }

  public function valid() {
    if (!isset($this->objects)) {
      $this->initQuery();
    }
    return $this->pos < count($this->objects);
  }

  public function getSchema() {
    return new SchemaArray(array(
      'entity_type' => $this->entityType,
      'limit'       => $this->limit,
      'offset'      => $this->offset,
    ));
  }

  public function restoreFromSchema(ReadonlySchemaInterface $config) {
    $this->entityType = $config->get('entity_type');
    $this->limit      = $config->get('limit');
    $this->offset     = $config->get('offset');
  }

  /**
   * Default constructor.
   * 
   * @param string $entityType = NULL
   * @param int $offset = NULL
   * @param int $limit = NULL
   */
  public function __construct($entityType = NULL, $offset = NULL, $limit = NULL) {
    $this->entityType = $entityType;
    $this->setRange($offset, $limit);
  }
}
