<?php

namespace ObjectStream;

use \Oox_Registrable_Abstract as RegistrableAbstract;

use \Iterator;

/**
 * Uses a PHP array as stream source.
 * 
 * <code>
 *   use ObjectStream;
 * 
 *   // You have an array of stuff you fetched back from somewhere, and you
 *   // wish to use it as an object stream in order to integrate with other
 *   // with other pieces of API, then just:
 *   $array = something();
 *   
 *   // This will create the stream instance using the datatype node.
 *   // 'raw' datatype is the default.
 *   $stream = new ArrayStream($array, 'node');
 * 
 *   // Then you can use it whenever you want.
 * </code>
 * 
 * The setLimit() and setOffset() methods here maybe have no sense at all since
 * you can pass the array you like at __construct() time. While this doesn't
 * really make a lot of sense, you can still use it. It will provide you paging
 * for your own array.
 */
class ArrayStream
  extends RegistrableAbstract
  implements Iterator, BrowseableStreamInterface
{
  /**
   * @var string
   */
  protected $datatype = OBJECT_STREAM_DATATYPE_RAW;

  /**
   * @var array
   */
  protected $objects;

  /**
   * For Iterable implementation only.
   * 
   * @var int
   */
  protected $pos = 0;

  /**
   * @var int
   */
  protected $limit = BrowseableStreamInterface::NO_LIMIT;

  /**
   * @var int
   */
  protected $offset = 0;

  /**
   * Allow user to pragmatically change datatype.
   * 
   * @param string $datatype
   * 
   * @return ArrayStream
   */
  public function setDatatype($datatype) {
    $this->datatype = $datatype;
    return $this;
  }

  public function getDatatype() {
    return $this->datatype;
  }

  public function setLimit($limit) {
    $this->limit = $limit;
    return $this;
  }

  public function getLimit() {
    return $this->limit;
  }

  public function setOffset($offset) {
    $this->offset = $offset;
    return $this;
  }

  public function getOffset() {
    return $this->offset;
  }

  public function setRange($offset = NULL, $limit = NULL) {
    if (isset($offset)) {
      $this->offset = $offset;
    }
    if (isset($limit)) {
      $this->limit = $limit;
    }
  }

  public function current() {
    return current($this->objects);
  }

  public function key() {
    return key($this->objects);
  }

  public function next() {
    next($this->objects);
    ++$this->pos;
  }

  public function rewind() {
    reset($this->objects);
    $this->pos = 0;
  }

  public function valid() {
    return $this->pos < count($this->objects);
  }

  public function count() {
    return count($this->objects);
  }

  /**
   * Default constructor.
   */
  public function __construct($objects = array(), $datatype = OBJECT_STREAM_DATATYPE_RAW) {
    $this->objects = $objects;
    $this->datatype = $datatype;
  }
}
