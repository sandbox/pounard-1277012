<?php

namespace ObjectStream\Filter;

/**
 * A filter is a particular object that is able to interfer with a specific
 * backend.
 * 
 * Filter does not provide any real interface, we only keep this interface for
 * registry operations and to ensure we have incorporated it in design since
 * the begining.
 * 
 * Each backend will provide its own filter interface, in order for the filter
 * to be in accordance with the backend internals for query alteration.
 * 
 * Remember we are not doing SQL queries here, the backend will determine the
 * real query nature, they can even provide no alteration at all and only
 * arbitrary result.
 * 
 * Filters are Oox_Optionable_Interface and eventual specific implementations
 * can provide the Oox_Formable_Interface interface for configuration. The
 * presence of this interface will be tested at runtime to build forms.
 * 
 * FIXME: Sorting is not part of the design, right now, therefore you can use
 * your filters for this.
 * 
 * Some filters may need to known the backend for some common operations, such
 * as formBuild(), any solution must be implemented in the specific backend
 * and filter implementation.
 */
interface FilterInterface {
  /**
   * Tells if the current filter can lead to random or pseudo random results.
   * This will be useful to determine dynamically consistent caching mecanism.
   * 
   * @return boolean
   */
  public function vary();

  /**
   * Tells if this filter can handle the given datatype.
   * 
   * @param string $datatype
   */
  public function handles($datatype);
}
