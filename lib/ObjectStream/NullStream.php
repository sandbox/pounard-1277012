<?php

namespace ObjectStream;

use \IteratorAggregate,
    \EmptyIterator;

/**
 * Null object implementation for object streams.
 */
class NullStream
  implements IteratorAggregate, StreamInterface
{
  public function getDatatype() {
    return 'null';
  }

  public function getIterator() {
    return new EmptyIterator;
  }
}
