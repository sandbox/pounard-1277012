<?php

namespace ObjectStream;

use \EntityFieldQuery as DrupalEntityFieldQuery;

/**
 * We may alter this object, to add really tricky condition, so let's keep an
 * override here, even if, at the moment, we don't have nothing to override.
 */
class EntityFieldQuery extends DrupalEntityFieldQuery {}
