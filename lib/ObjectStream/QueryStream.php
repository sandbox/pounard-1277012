<?php

namespace ObjectStream;

use \SelectQuery as SelectQuery;

/**
 * Uses a SelectQuery object as stream source.
 * 
 * For example, if you are loading node entities, ensure that your query only
 * does fetch the nid field, then you can do:
 * 
 * <code>
 *   use ObjectStream;
 * 
 *   // With single object processor.
 *   $stream = new QueryStream;
 *   $stream
 *     ->setDatatype('node')
 *     ->setQuery(db_select('node', 'n')->fields('n'))
 *     ->setLimit(10)
 *     ->setProcessor('node_load', FALSE);
 *   foreach ($stream as $node) {
 *     // Do something.
 *   }
 * 
 *   // With multiple object processor, this method is faster.
 *   $stream = new QueryStream;
 *   $stream
 *     ->setDatatype('node')
 *     ->setQuery(db_select('node', 'n')->fields('n'))
 *     ->setLimit(10)
 *     ->setProcessor('node_load_multiple', TRUE);
 *   foreach ($stream as $node) {
 *     // Do something.
 *   }
 * 
 *   // Both here give the exact same result.
 *   
 *   // Another example for fun.
 *   $query = db_select('some_entity_table') // ... 
 *   $stream = new QueryStream($query, 'some_entity_load_multiple', TRUE);
 *   foreach ($stream as $some_entity_instance) {
 *     // Do something.
 *   }
 * 
 * </code>
 * 
 * Notice that calls to setDatatype() method is optional. It is useful
 * only if you intend to use the stream object in an higher level API
 * that needs to have a pragmaticaly set datatype.
 * 
 * Also notice that this will work because nid will be the first column
 * naturally, but if it didn't, the node_load() and node_load_multiple()
 * function calls would have failed.
 * 
 * Also notice that this class does not support EntityFieldQuery, but only
 * custom queries instead. For EntityFieldQuery, use the EntityQueryStream
 * class instead.
 * 
 * This class can use a provided query, but if you need to an implementation
 * that always does the same query, or with different options, you can extend
 * it and override the getQuery() or __construct() methods to do change the
 * way you create the query (i.e. pragmatically create a query for example).
 * 
 * Sadly, we cannot use closures here, which could have been a great feature.
 * With PHP 5.3, callbacks also contains closures and objects that implements
 * the __invoke() magic method.
 */
class QueryStream extends ArrayStream {
  /**
   * Fetches each object from query as an stdClass.
   */
  const FETCH_OBJECT = 0;

  /**
   * Fetches each object from query as an array.
   */
  const FETCH_ARRAY = 1;

  /**
   * Fetches only the first field of given query.
   */
  const FETCH_FIELD = 2;

  /**
   * @var bool
   */
  protected $run = FALSE;

  /**
   * @var int
   */
  protected $count;

  /**
   * @var SelectQuery
   */
  protected $query;

  /**
   * @var callback
   */
  protected $processor;

  /**
   * @var callback
   */
  protected $processMultiple = FALSE;

  /**
   * @var int
   */
  protected $method = self::FETCH_OBJECT;

  /**
   * Set query.
   * 
   * @param SelectQuery $query
   * 
   * @return QueryStream
   */
  public function setQuery(SelectQuery $query) {
    $this->query = $query;
    return $this;
  }

  /**
   * Get query.
   * 
   * @return SelectQuery
   */
  public function getQuery() {
    return $this->query;
  }

  /**
   * Set processor function.
   * 
   * @param callback $processor
   * @param boolean $processMultiple = FALSE
   * @param int $method = QueryStream::FETCH_FIELD
   *   (optional) Per default, we consider that most *_load() functions will
   *   need only identifiers to load the objects, that's why we override it
   *   this way when the user sets a processor callback.
   * 
   * @return QueryStream
   */
  public function setProcessor($processor, $processMultiple = FALSE, $method = QueryStream::FETCH_FIELD) {
    $this->processor = $processor;
    $this->processMultiple = $processMultiple;
    $this->method = $method;
    return $this;
  }

  /**
   * Prepare and execute the query.
   * 
   * @param boolean $force = FALSE
   *   (optional) If set to TRUE, query will be run again.
   * 
   * @return QueryStream
   */
  protected function executeQuery($force = FALSE) {
    if (!$force && $this->run) {
      return;
    }

    if (!$query = $this->getQuery()) {
      throw new Exception("Query is not set.");
    }

    // Reset internals.
    $this->objects = array();

    // Set some stuff.
    if (0 !== $this->limit) {
      $query->range($this->offset, $this->limit);
    }

    $result = $query->execute();

    switch ($this->method) {
      case self::FETCH_OBJECT:
        $this->objects = $result->fetchAll();
        break;

      case self::FETCH_ARRAY:
        while ($array = $result->fetchAssoc()) {
          $this->objects[] = $array;
        }
        break;

      case self::FETCH_FIELD:
        $this->objects = $result->fetchCol();
        break;

      default:
        throw new Exception("Unknown fetch method.");
    }

    // Process if needed.
    if (is_callable($this->processor) && !empty($this->objects)) {
      if ($this->processMultiple) {
        $this->objects = call_user_func($this->processor, $this->objects);
      } else {
        foreach ($this->objects as $key => $object) {
          $this->objects[$key] = call_user_func($this->processor, $object);
        }
      }
    }

    $this->run = TRUE;  
  }

  /**
   * Prepare and execute the count query.
   * 
   * @param boolean $force = FALSE
   *   (optional) If set to TRUE, query will be run again.
   * 
   * @return QueryStream
   */
  public function countQuery() {
    // The isset() call here is mandatory, because count could be zero.
    if (!$force && isset($this->count)) {
      return;
    }

    if (!$query = $this->getQuery()) {
      throw new Exception("Query is not set.");
    }

    // Do not modify the original query.
    $this->count = $query
      ->range()
      ->countQuery()
      ->execute()
      ->fetchField();

    return $this;
  }

  /**
   * Forces the query to be run at rewind() time.
   * 
   * @see ArrayStream::rewind()
   */
  public function rewind() {
    $this->executeQuery();
    parent::rewind();
  }

  /**
   * Forces query to be prepared, and do a count query instead for fetching
   * the count result.
   * 
   * @see Countable::count()
   */
  public function count() {
    $this->countQuery();
    return $this->count;
  }

  /**
   * Default constructor.
   * 
   * @param SelectQuery $query = NULL
   *   Query that fetches data.
   * @param callback $processor = NULL
   *   Callback that will process each object before it's being
   *   returned by the Iterable interface.
   * @param boolean $processMultiple = FALSE
   *   If set to TRUE, the callback will be called using the 
   */
  public function __construct(SelectQuery $query = NULL, $processor = NULL, $processMultiple = FALSE) {
    $this->query = $query;
    $this->processor = $processor;
    $this->processMultiple = $processMultiple;

    // Force fetch style to be field, see PHP-doc on setProcessor() method.
    if (is_callable($this->processor)) {
      $method = self::FETCH_FIELD;
    }
  }
}
