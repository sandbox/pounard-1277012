<?php
// $Id$

/**
 * Implementation of ObjectStream_Backend_Interface using the
 * Xoxo_Object_Exportable class as base, which provides
 * Oox_Registrable_Abstract and Oox_Optionable
 * implementations as well for data storage.
 */
abstract class ObjectStream_Backend_Persistent_Abstract
  extends Xoxo_Object_Exportable
  implements ObjectStream_Backend_Interface
{
  /**
   * @see ObjectStream_Backend_Interface::setDatatype()
   */
  public function setDatatype($datatype) {
    if (!$this->canFetch($datatype)) {
      throw new ObjectStream_Exception("Datatype " . $datatype . " is not supported by this implementation");
    }
    $this->_options['datatype'] = $datatype;
    return $this;
  }

  /**
   * @see ObjectStream_Backend_Interface::getDatatype()
   */
  public function getDatatype() {
    return $this->_options['datatype'];
  }

  /**
   * @see ObjectStream_Backend_Interface::removeFilter()
   */
  /*
  public function removeFilter(ObjectStream_Filter_Interface $filter) {
    foreach ($this->_options['filters'] as $key => $_filter) {
      if ($filter == $_filter) {
        unset($this->_options['filters'][$key]);
      }
    }
    return $this;
  }
   */

  /**
   * @see ObjectStream_Backend_Interface::getFilters()
   */
  /*
  public function getFilters() {
    return $this->_options['filters'];
  }
   */

  /**
   * @see ObjectStream_Backend_Interface::addFilter()
   */
  /*
  public function addFilter(ObjectStream_Filter_Interface $filter) {
    if (!$this->_options['datatype']) {
      throw new ObjectStream_Exception("Object has no datatype therefore adding a filter can not be done.");
    }
    foreach ($this->_options['filters'] as $_filter) {
      if ($filter == $_filter) {
        // Filter already set.
        return;
      }
    }
    $this->_options['filters'][] = $filter;
    return $this;
  }
   */

  /**
   * @see Xoxo_Object::init()
   */
  public function init(Schema_Interface $config = NULL) {
    parent::init($config);
    $this->_options['filters'] = array();
  }

  /**
   * @see Formable_Interface::getForm()
   */
  public function getForm($formIdentifier = Formable_Interface::FORM_DEFAULT) {
    if (Formable_Interface::FORM_DEFAULT == $formIdentifier) {
      return new ObjectStream_Backend_Persistent_AbstractFormDefault;
    }
  }
}
