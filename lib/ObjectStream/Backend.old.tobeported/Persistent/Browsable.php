<?php
// $Id$

/**
 * Implementation of Vc_Backend_Browsable_Interface using the
 * Xoxo_Object_Exportable class as base, which provides the
 * Oox_Registrable_Abstract and Oox_Optionable implementations
 * as well for data storage.
 */
abstract class Vc_Backend_Persistent_Browsable extends Vc_Backend_Persistent_Abstract implements Oox_ObjectStream_Browsable_Interface
{
  /**
   * @see Vc_Backend_Browsable_Interface::setLimit()
   */
  public function setLimit($limit) {
    $this->_options['limit'] = $limit;
    return $this;
  }

  /**
   * @see Vc_Backend_Browsable_Interface::getLimit()
   */
  public function getLimit() {
    return $this->_options['limit'];
  }

  /**
   * @see Vc_Backend_Browsable_Interface::setOffset()
   */
  public function setOffset($offset) {
    $this->_options['offset'] = $offset;
    return $this;
  }

  /**
   * Get current query offset.
   * 
   * @return int
   */
  public function getOffset() {
    return $this->_options['offset'];
  }

  /**
   * @see Xoxo_Object::init()
   */
  public function init(Schema_Interface $config = NULL) {
    parent::init($config);
    $this->_options['limit'] = Oox_ObjectStream_Browsable_Interface::NO_LIMIT;
    $this->_options['offset'] = 0;
  }
}
