<?php
// $Id$

/**
 * ObjectStream_Backend_Persistent_Abstract form builder.
 */
class ObjectStream_Backend_Persistent_AbstractFormDefault
  extends Xoxo_Object_ExportableFormDefault
{
  /**
   * @see Xoxo_Object_Exportable::formBuild()
   */
  public function formBuild(array &$form, array $values = array(), $parent = NULL) {
    parent::formBuild($form, $values, $parent);

    /*
    if ($this->getFilterInterface()) {
      // Filters section.
      $form['filters'] = array(
        '#type' => 'fieldset',
        '#title' => t("Filters"),
        '#group' => 'components',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#description' => t("Backend filters."),
      );
      $form['filters']['filters'] = array(
        '#prefix' => '<div class="clearfix" id="filters-wrapper">',
        '#suffix' => '</div>',
      );
      $form['filters']['filters']['add'] = array(
        '#type' => 'oox_registry_item',
        '#title' => t("Add a new filter"),
        '#registry_type' => 'obstream_filter',
        '#input_type' => 'select',
        '#registry_cb' => 'getPotentialItemsFor',
        '#registry_cb_args' => array($this),
      );
      $form['filters']['filters']['add_submit'] = array(
        '#type' => 'submit',
        '#value' => t("Add"),
        '#submit' => array('obstream_backend_abstract_form_filters_add_submit'),
        '#limit_validation_errors' => array(array('object_form', 'filters')),
        '#attributes' => array('class' => array('use-ajax-submit')),
        '#ajax' => array(
          'callback' => 'obstream_backend_abstract_form_filters_js',
          'wrapper' => 'filters-wrapper',
        ),
      );

      $items = array();
  
      // And this where the magic happens.
      foreach ($this->getFilters() as $delta => $filter) {
        // This property will be used by the xoxo_mutiple_object_form theme
        // function. It makes no sense otherwise.
        $items[$delta]['title'] = array('#markup' => '<h3>' . $filter->getRegistry()->getItemName($filter) . '</h3>');
  
        if ($filter instanceof Oox_Formable_Interface) {
          $items[$delta]['item'] = array(
            '#type' => 'oox_formable',
            '#object' => $filter,
          );
        }
  
        $items[$delta]['remove-' . $delta] = array(
          '#type' => 'submit',
          '#value' => t("Remove"),
          '#submit' => array('obstream_backend_abstract_form_filters_remove_submit'),
          '#limit_validation_errors' => array(array('object_form', 'filters')),
          '#attributes' => array('class' => array('use-ajax-submit')),
          '#ajax' => array(
            'callback' => 'obstream_backend_abstract_form_filters_js',
            'wrapper' => 'filters-wrapper',
          ),
        );
      }
  
      // This is where the real items will live.
      $form['filters']['filters'] += array(
        '#theme' => 'xoxo_multiple_object_form',
        'items' => $items,
      );
    }
     */
  }
}

/**
 * Add decorator submit handler.
 */
function obstream_backend_abstract_form_filters_add_submit($form, &$form_state) {
  // FIXME: We now this is the path because it's written in the xoxo.admin.inc
  // file, but the Oox_Formable_Interface API should be path agnostic, and
  // should propose helpers to find out the real path, like by playing with the
  // form state array to set tags.
  $path = array('object_form');

  // FIXME: Once more, this is not written in the full pattern, we should
  // consider reworking this part.
  $object = $form_state['object'];

  // Get current select value.
  if ($type = $form_state['input']['object_form']['object']['filters']['filters']['add']['type']) {
    try {
      $item = oox_registry_get('obstream_filter')->getItem($type);
      $object->addFilter($item);
    }
    catch (Oox_Exception $e) {
      // We should watchdog what happend here.
      die($e);
    }
  }

  // Mark the form for rebuild. The object will give the new decorator list to
  // build state.
  $form_state['rebuild'] = TRUE;
}

/**
 * Add decorator submit handler.
 */
function obstream_backend_abstract_form_filters_remove_submit($form, &$form_state) {
  $object = $form_state['object'];

  // Fetch decorator delta to remove.
  // Proceed to variable recopy to avoid modifications.
  $parents = $form_state['clicked_button']['#parents'];
  list(, $delta) = explode('-', array_pop($parents), 2);

  // Ulgy part of the algorithm.
  foreach ($object->getFilters() as $i => $filter) {
    if ($i == $delta) {
      $object->removeFilter($filter);
      break;
    }
  }

  // Mark the form for rebuild. The object will give the new decorator list to
  // build state.
  $form_state['rebuild'] = TRUE;
}

/**
 * Add decorator AJAX return callback.
 */
function obstream_backend_abstract_form_filters_js($form, $form_state) {
  return $form['object_form']['object']['filters']['filters'];
}
