<?php
// $Id$

/**
 * Database table representation.
 * 
 * Properties are public, this is totally normal. In other languages such as
 * C# or Java this would be a protected class, that would exists in the
 * ObjectStream_Backend_Database namespace.
 */
class ObjectStream_Backend_Database_Table implements ObjectStream_Backend_Database_Statement
{
  /**
   * @var string
   */
  public $table;

  /**
   * @var string
   */
  public $alias;

  /**
   * @var ObjectStream_Backend_Database_Join
   */
  public $join;

  /**
   * Default constructor.
   */
  public function __construct($table, $alias = NULL, $join = NULL) {
    $this->name = $table;
    $this->alias = isset($alias) ? $alias : $table;
  }
}
