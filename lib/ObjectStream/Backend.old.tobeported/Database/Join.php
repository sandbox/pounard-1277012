<?php
// $Id$

/**
 * Database join representation.
 * 
 * Properties are public, this is totally normal. In other languages such as
 * C# or Java this would be a protected class, that would exists in the
 * ObjectStream_Backend_Database namespace.
 */
class ObjectStream_Backend_Database_Join
{
  const _DEFAULT = NULL;

  const INNER = 'INNER';

  const NATURAL = 'NATURAL';

  const LEFT = 'LEFT';

  const INNER = 'INNER';

  const OUTER = 'OUTER';

  /**
   * @var ObjectStream_Backend_Database_Join
   */
  public $leftTable;

  /**
   * @var ObjectStream_Backend_Database_Join
   */
  public $rightTable;

  /**
   * @var string
   */
  public $leftField;

  /**
   * @var string
   */
  public $rightField;

  /**
   * @var string
   */
  public $type;

  /**
   * Default constructor.
   */
  public function __construct(ObjectStream_Backend_Database_Join $leftTable, ObjectStream_Backend_Database_Join $rightTable, $leftField, $rightField, $type = self::_DEFAULT) {
    $this->leftTable = $leftTable;
    $this->rightTable = $rightTable;
    $this->leftField = $leftField;
    $this->rightField = $rightField;
    $this->type = $type;
  }
}
