<?php
// $Id$

/**
 * Class being used using the proxy pattern into ObjectStream_Backend_Database
 * implementation in order to exclude the code which is quite heavy from
 * the backend class.
 * 
 * Most backend loading will only ends up with data query, so we don't
 * need those forms when loading this stuff.
 */
class ObjectStream_Backend_Database_Form
{
  /**
   * @var ObjectStream_Backend_Database
   */
  protected $_backend;

  /**
   * Constructor.
   * 
   * @param ObjectStream_Backend_Database $backend
   */
  public function __construct(ObjectStream_Backend_Database $backend) {
    $this->_backend = $backend;
  }

  public function tableForm(array &$form, array $values = array()) {
    
  }

  public function tableFormValidate(array &$form, array $values = array()) {
    return array();
  }

  public function tableFormSubmit(array &$form, array $values = array()) {
    
  }

  public function selectForm(array &$form, array $values = array()) {
    
  }

  public function selectFormValidate(array &$form, array $values = array()) {
    return array();
  }

  public function selectFormSubmit(array &$form, array $values = array()) {
    
  }

  public function whereForm(array &$form, array $values = array()) {
    
  }

  public function whereFormValidate(array &$form, array $values = array()) {
    return array();
  }

  public function whereFormSubmit(array &$form, array $values = array()) {
    
  }

  public function miscForm(array &$form, array $values = array()) {
    
  }

  public function miscFormValidate(array &$form, array $values = array()) {
    return array();
  }

  public function miscFormSubmit(array &$form, array $values = array()) {
    
  }
}
