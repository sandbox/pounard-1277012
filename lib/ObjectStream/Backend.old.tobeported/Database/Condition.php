<?php
// $Id$

/**
 * Database condition representation.
 * 
 * Properties are public, this is totally normal. In other languages such as
 * C# or Java this would be a protected class, that would exists in the
 * ObjectStream_Backend_Database namespace.
 */
class ObjectStream_Backend_Database_Condition
{
  /**
   * @var string
   */
  public $leftField;

  /**
   * @var string
   */
  public $leftTableAlias;

  /**
   * Default constructor.
   */
  public function __construct($leftStatement, $rightStatement, $operator) {
    $this->name = $name;
    $this->tableAlias = $tableAlias;
    if (isset($alias)) {
      $this->alias = $alias;
    }
  }

  /**
   * Return the SELECT field as string.
   * 
   * @return string
   */
  public function __toString() {
    return $this->tableAlias . '.' .  $this->name . (isset($this->alias) ? ' AS ' . $this->alias : '');  
  }
}
