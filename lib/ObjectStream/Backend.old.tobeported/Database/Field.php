<?php
// $Id$

/**
 * Database field representation. First iteration will provide living instances
 * for each field. May be for optimization needs, in the second, we'll implement
 * the flyweight pattern.
 * 
 * Properties are public, this is totally normal. In other languages such as
 * C# or Java this would be a protected class, that would exists in the
 * ObjectStream_Backend_Database namespace.
 */
class ObjectStream_Backend_Database_Field implements ObjectStream_Backend_Database_Statement
{
  /**
   * @var string
   */
  public $name;

  /**
   * @var string
   */
  public $alias;

  /**
   * @var string
   */
  public $tableAlias;

  /**
   * Default constructor.
   */
  public function __construct($name, $tableAlias, $alias = NULL) {
    $this->name = $name;
    $this->tableAlias = $tableAlias;
    if (isset($alias)) {
      $this->alias = $alias;
    }
  }

  /**
   * Return the SELECT field as string.
   * 
   * @return string
   */
  public function __toString() {
    return $this->tableAlias . '.' .  $this->name . (isset($this->alias) ? ' AS ' . $this->alias : '');  
  }
}
