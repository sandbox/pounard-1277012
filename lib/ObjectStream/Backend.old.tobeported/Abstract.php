<?php
// $Id$

/**
 * Base implementation for Vc_Backend_Interface.
 */
abstract class Vc_Backend_Abstract extends Oox_Registrable_Abstract implements Vc_Backend_Interface
{
  /**
   * @var string
   */
  protected $_datatype;

  /**
   * @see Vc_Backend_Interface::setDatatype()
   */
  public function setDatatype($datatype) {
    if (!$this->canFetch($datatype)) {
      throw new Vc_Exception("Datatype " . $datatype . " is not supported by this implementation");
    }
    $this->_datatype = $datatype;
    return $this;
  }

  /**
   * @see Vc_Backend_Interface::getDatatype()
   */
  public function getDatatype() {
    return $this->_datatype;
  }

  /**
   * @var array
   *   Array of Vc_Filter_Interface instances.
   */
  /*
  protected $_filters = array();
   */

  /**
   * @see Vc_Backend_Interface::removeFilter()
   */
  /*
  public function removeFilter($filter) {
    foreach ($this->_filters as $key => $_filter) {
      if ($filter == $_filter) {
        unset($this->_filters[$key]);
      }
    }
    return $this;
  }
   */

  /**
   * @see Vc_Backend_Interface::removeFilterAt()
   */
  /*
  public function removeFilterAt($index) {
    unset($this->_filters[$index]);
  }
   */

  /**
   * @see Vc_Backend_Interface::getFilterAt()
   */
  /*
  public function getFilterAt($index) {
    if (!isset($this->_filters[$index])) {
      throw new Vc_Exception("No filter set at index " . $index);
    }
    return $this->_filters[$index];
  }
   */

  /**
   * @see Vc_Backend_Interface::getFilters()
   */
  /*
  public function getFilters() {
    return $this->_filters;
  }
   */

  /**
   * @see Vc_Backend_Interface::addFilter()
   */
  /*
  public function addFilter(Vc_Filter_Interface $filter) {
    if (!$this->_datatype) {
      throw new Vc_Exception("Object has no datatype therefore adding a filter can not be done.");
    }
    foreach ($this->_filters as $_filter) {
      if ($filter == $_filter) {
        // Filter already set.
        return;
      }
    }
    $this->_filters[] = $filter;
    return $this;
  }
  */
}