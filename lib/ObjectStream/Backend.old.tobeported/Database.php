<?php
// $Id$

/**
 * Arbitrary build database queries, graphically. This means you will able
 * to create complex database queries then display the raw data them as you
 * want.
 * 
 * This particular implementation will allow some filters to exists, but will
 * primarly help you creating complex SELECT statement, using JOIN and stuff
 * that matches that.
 * 
 * May be later, it could even use Views module data description to ensure
 * valid joins and select.
 */
class Vc_Backend_Database extends Vc_Backend_Persistent_Browsable
{
  /**
   * Get tables.
   * 
   * @return array
   *   Array of tables.
   */
  public function getTables() {
    if ($this->_options['tables']) {
      return $this->_options['tables'];
    }
    return array();
  }

  /**
   * Add table.
   * 
   * @param Vc_Backend_Database_Table $table
   */
  public function addTable(Vc_Backend_Database_Table $table) {
    $this->_options['tables'][] = $table;
  }

  /**
   * @see Vc_Backend_Interface::canFetch()
   */
  public function canFetch($datatype) {
    if ($datatype == VC_RAW_DATATYPE) {
      return TRUE;
    }
    // FIXME: Introspect schema API, and get back all tables.
    // Some of them would then be associated to particular datatypes.
  }

  /**
   * @see Vc_Backend_Persistent_Abstract::formBuild()
   */
  public function formBuild(array &$form, array $values = array()) {
    parent::formBuild($form, $values);

    $helper = new Vc_Backend_Database_Form($this);

    $form['tables'] = array(
      '#type' => 'fieldset',
      '#title' => t("Tables (FROM and JOIN statements)"),
      '#group' => 'components',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $helper->tableForm($form['tables'], isset($values['tables']) ? $values['table'] : array());

    $form['select'] = array(
      '#type' => 'fieldset',
      '#title' => t("Fields (SELECT clause)"),
      '#group' => 'components',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $helper->tableForm($form['select'], isset($values['select']) ? $values['select'] : array());

    $form['where'] = array(
      '#type' => 'fieldset',
      '#title' => t("Filters (WHERE statements)"),
      '#group' => 'components',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $helper->whereForm($form['where'], isset($values['where']) ? $values['where'] : array());

    $form['misc'] = array(
      '#type' => 'fieldset',
      '#title' => t("HAVING, GROUP BY, ORDER"),
      '#group' => 'components',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $helper->miscForm($form['misc'], isset($values['misc']) ? $values['misc'] : array());
  }

  /**
   * @see Oox_Formable_Interface::formValidate()
   */
  public function formValidate(array &$values) {
    if (!$errors = parent::formSubmit($values)) {
      $errors = array();
    }
    $helper = new Vc_Backend_Database_Form($this);
    $errors = array_merge($errors, $helper->tableFormValidate($form['tables'], $values['tables']));
    $errors = array_merge($errors, $helper->tableFormValidate($form['select'], $values['select']));
    $errors = array_merge($errors, $helper->whereFormValidate($form['where'], $values['where']));
    $errors = array_merge($errors, $helper->miscFormValidate($form['misc'], $values['misc']));
    return $errors;
  }

  /**
   * @see Oox_Formable_Interface::formSubmit()
   */
  public function formSubmit(array &$values) {
    parent::formSubmit($values);
    $helper = new Vc_Backend_Database_Form($this);
    $errors = array_merge($errors, $helper->tableFormSubmit($form['tables'], $values['tables']));
    $errors = array_merge($errors, $helper->tableFormSubmit($form['select'], $values['select']));
    $errors = array_merge($errors, $helper->whereFormSubmit($form['where'], $values['where']));
    $errors = array_merge($errors, $helper->miscFormSubmit($form['misc'], $values['misc']));
  }

  /**
   * @see Vc_Backend_Interface::query()
   */
  public function query() {
    throw new Exception("Not implemented.");
  }

  /**
   * @see Iterator::current()
   */
  public function current() {
    throw new Exception("Not implemented.");
  }

  /**
   * @see Iterator::next()
   */
  public function next() {
    throw new Exception("Not implemented.");
  }

  /**
   * @see Iterator::key()
   */
  public function key() {
    throw new Exception("Not implemented.");
  }

  /**
   * @see Iterator::valid()
   */
  public function valid() {
    throw new Exception("Not implemented.");
  }

  /**
   * @see Iterator::rewind()
   */
  public function rewind() {
    throw new Exception("Not implemented.");
  }

  /**
   * @see Countable::count()
   */
  public function count () {
    throw new Exception("Not implemented.");
  }

  /**
   * Special case, for this and this only, we don't want any filter to exists.
   * 
   * @see Vc_Backend_Interface::getFilterInterface()
   */
  public function getFilterInterface() {
    return NULL;
  }
}
