<?php
// $Id$

/**
 * A backend is a plugin which will allow particular requests to fetch data
 * from a specific source.
 * 
 * The backend determine the data nature you can fetch, but also the enabled
 * filters you can use for your request.
 * 
 * Limit and offset parameters are volatile options. It does not make any
 * sens to store those. Each particular backend has the responsability to
 * manipulate those by itself.
 * 
 * Theoratically, this interface should not exists, only a core implementation
 * would have sens here. But as this is pure object, you will be able to use
 * any other implementation, such as implements using no backend at all. This
 * will allow you to return arbitrary data set.
 * 
 * Use the Iterator interface for browsing through results.
 * 
 * This object must be manipulated as a stream.
 *
 * Here is a quick start explanation about different backend interfaces and
 * abstract classes this module provides:
 * 
 *  - Vc_Backend_Interface : Any backend usable by the system should implement
 *    this interface. Backends are basically typed data stream.
 * 
 *  - Vc_Backend_Browsable_Interface : Extends the Vc_Backend_Interface interface by
 *    allowing implementations to be browsable. This means stream can be fetch
 *    using limit and offset parameters, allowing paged queries. Most backends
 *    will use this interface.
 * 
 *  - Vc_Backend_Abstract : Abstract implementation of Vc_Backend_Interface, inherit from
 *    this if you have basic needs.
 * 
 *  - Vc_Backend_Browsable_Abstract : Abstract implementation of
 *    Vc_Backend_Browsable_Interface, inherit from this if you have basic needs
 *    and you can allow paged requests.
 * 
 *  - Vc_Backend_Persistent_Abstract : Abstract implementation of Vc_Backend_Interface,
 *    using a different property storage through the Oox_Optionable_Interface
 *    interface, and extending the Xoxo_Object_Exportable class, which makes it
 *    elligible for administration screen configuration and persistence. Most
 *    implementations would probably need this one to implement a configurable
 *    and reusable basic data stream implementation.
 * 
 *  - Vc_Backend_Persistent_Browsable : Same as the first above, extending
 *    Vc_Backend_Browsable_Interface instead of Vc_Backend_Interface.
 *  
 *  Depending on your needs, remember that the simplest base implementation
 *  you choose will always be the fastest. Don't use persistent implementation
 *  if you don't need to, i.e. if you need to use your backend as pure API and
 *  don't want to expose it to final users.
 */
interface ObjectStream_Backend_Interface extends Oox_Registrable_Interface, ObjectStream_Interface
{
  /**
   * Set datatype to fetch.
   * 
   * @param string $datatype
   * 
   * @return Obstream_Persistent_Interface
   * 
   * @throws Oox_Exception
   *   If datatype is not supported.
   */
  public function setDatatype($datatype);

  /**
   * Remove given filter.
   * 
   * @return Obstream_Persistent_Interface
   *   Self reference for chaining.
   */
//  public function removeFilter(Vc_Filter_Interface $filter);

  /**
   * Return all backend filters.
   * 
   * @return array
   *   Array of Vc_Filter_Interface instances.
   */
//  public function getFilters();

  /**
   * Add filter.
   * 
   * @param Vc_Filter_Interface $filter
   * 
   * @return Vc_Backend_Interface
   *   Self reference for chaining.
   *
   * @throws Vc_Exception
   *   If no datatype has been set.
   */
//  public function addFilter(Vc_Filter_Interface $filter);

  /**
   * Return default filter interface for given backend. Any filter implementing
   * the given will be then be able to be used with this backend.
   * 
   * You return only one interface here, it can be dependent on the current
   * datatype, it will be called only if this property has been set.
   * 
   * @return string
   *   Interface or class name, extending or implementing the Vc_Filter_Interface
   *   interface. 
   */
//  public function getFilterInterface();

  /**
   * Tells if the current backend can fetch objects of the given type.
   * 
   * @param string $datatype
   *   Datatype the backend will fetch.
   * 
   * @return boolean
   */
  public function canFetch($datatype);

  /**
   * Initiliazed the object, and execute the query. Each time you call this
   * method new parameters are being used and query can be different. The
   * request object can be used and re-used many times.
   * 
   * This must be called before each foreach() usage over the Iterator interface
   * else it will throw exceptions at foreach() attempt. This will ensure that
   * no query will ever be called outside the right execution workflow and
   * ensure code reliability.
   * 
   * @return Obstream_Persistent_Interface
   * 
   * @throws Oox_Exception
   *   In case of any underlaying layer error.
   */
  public function query();
}
