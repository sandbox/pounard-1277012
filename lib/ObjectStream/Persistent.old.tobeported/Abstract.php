<?php
// $Id$

/**
 * Implementation of ObjectStream_Interface using the Xoxo_Object_Exportable
 * class as base, which provides Oox_Registrable_Abstract implementation for
 * data storage.
 */
abstract class ObjectStream_Persistent_Abstract
  extends Xoxo_Object_Exportable
  implements ObjectStream_Interface
{
}
