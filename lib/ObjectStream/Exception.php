<?php

namespace ObjectStream;

use \RuntimeException;

class Exception extends RuntimeException {}
